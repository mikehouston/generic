package list

import "bitbucket.org/mikehouston/generic"

type ListItem interface{}

type List struct {
	T ListItem

	Len    func() int
	Get    func(i int) ListItem
	Set    func(i int, item ListItem) ListItem
	Append func(item ListItem)
	Insert func(i int, item ListItem)
	Remove func(i int) ListItem
}

type Int64List struct {
	T int64

	Len    func() int
	Get    func(i int) int64
	Set    func(i int, item int64) int64
	Append func(item int64)
	Insert func(i int, item int64)
	Remove func(i int) int64
}

type Int32List struct {
	T int32

	Len    func() int
	Get    func(i int) int32
	Set    func(i int, item int32) int32
	Append func(item int32)
	Insert func(i int, item int32)
	Remove func(i int) int32
}

type StringList struct {
	T string

	Len    func() int
	Get    func(i int) string
	Set    func(i int, item string) string
	Append func(item string)
	Insert func(i int, item string)
	Remove func(i int) string
}

func init() {
	generic.Factory(List{}).
		Register(ListImpl{}, newList).
		Register(StringListImpl{}, newStringList).
		Register(Int32ListImpl{}, newInt32List)
}

type ListImpl struct {
	T     ListItem
	items []ListItem
}

func newList() interface{} {
	return &ListImpl{}
}

func (l *ListImpl) Len() int {
	return len(l.items)
}

func (l *ListImpl) Get(i int) ListItem {
	return l.items[i]
}

func (l *ListImpl) Set(i int, item ListItem) ListItem {
	existing := l.items[i]
	l.items[i] = item
	return existing
}

func (l *ListImpl) Append(item ListItem) {
	l.items = append(l.items, item)
}

func (l *ListImpl) Insert(i int, item ListItem) {
	l.items = append(l.items[:i], append([]ListItem{item}, l.items[i:]...)...)
}

func (l *ListImpl) Remove(i int) ListItem {
	existing := l.items[i]
	l.items = append(l.items[:i], l.items[i+1:]...)
	return existing
}

type StringListImpl struct {
	T     string
	items []string
}

func newStringList() interface{} {
	return &StringListImpl{}
}

func (l *StringListImpl) Len() int {
	return len(l.items)
}

func (l *StringListImpl) Get(i int) string {
	return l.items[i]
}

func (l *StringListImpl) Set(i int, item string) string {
	existing := l.items[i]
	l.items[i] = item
	return existing
}

func (l *StringListImpl) Append(item string) {
	l.items = append(l.items, item)
}

func (l *StringListImpl) Insert(i int, item string) {
	l.items = append(l.items[:i], append([]string{item}, l.items[i:]...)...)
}

func (l *StringListImpl) Remove(i int) string {
	existing := l.items[i]
	l.items = append(l.items[:i], l.items[i+1:]...)
	return existing
}

type Int32ListImpl struct {
	T     int32
	items []int32
}

func newInt32List() interface{} {
	return &Int32ListImpl{}
}

func (l *Int32ListImpl) Len() int {
	return len(l.items)
}

func (l *Int32ListImpl) Get(i int) int32 {
	return l.items[i]
}

func (l *Int32ListImpl) Set(i int, item int32) int32 {
	existing := l.items[i]
	l.items[i] = item
	return existing
}

func (l *Int32ListImpl) Append(item int32) {
	l.items = append(l.items, item)
}

func (l *Int32ListImpl) Insert(i int, item int32) {
	l.items = append(l.items[:i], append([]int32{item}, l.items[i:]...)...)
}

func (l *Int32ListImpl) Remove(i int) int32 {
	existing := l.items[i]
	l.items = append(l.items[:i], l.items[i+1:]...)
	return existing
}
