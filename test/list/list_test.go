package list

import (
	"reflect"
	"testing"

	"bitbucket.org/mikehouston/generic"
)

func TestAdaptGenericList(t *testing.T) {
	l := &Int64List{}
	generic.Factory(List{}).Create(l)

	for i := 0; i < 1000; i++ {
		l.Append(int64(i))
		if l.Get(i) != int64(i) {
			t.Fatal("Unable to retrieve value from generic list")
		}
	}
}

func TestAdaptStringList(t *testing.T) {
	l := &StringList{}
	generic.Factory(List{}).Create(l)

	for i := 0; i < 1000; i++ {
		l.Append("a")
		if l.Get(i) != "a" {
			t.Fatal("Unable to retrieve value from string list")
		}
	}
}

func TestAdaptInt32List(t *testing.T) {
	l := &Int32List{}
	generic.Factory(List{}).Create(l)

	for i := 0; i < 1000; i++ {
		l.Append(int32(i))
		if l.Get(i) != int32(i) {
			t.Fatal("Unable to retrieve value from int32 list")
		}
	}
}

func BenchmarkDirectListCreate(b *testing.B) {
	for i := 0; i < b.N; i++ {
		l := newInt32List()
		_ = l
	}
}

func BenchmarkDirectListAppend(b *testing.B) {
	l := &Int32ListImpl{}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		l.Append(int32(i))
	}
}

func BenchmarkDirectListSet(b *testing.B) {
	l := &Int32ListImpl{}
	l.Append(1)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		l.Set(0, 5)
	}
}

func BenchmarkDirectListGet(b *testing.B) {
	l := &Int32ListImpl{}
	l.Append(1)
	var v int32

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v = l.Get(0)
	}
	if v != 1 {
		panic("Slice value doesn't match expected")
	}
}

func BenchmarkGenericListCreate(b *testing.B) {
	for i := 0; i < b.N; i++ {
		l := &Int64List{}
		generic.Factory(List{}).Create(l)
	}
}

func BenchmarkGenericListAppend(b *testing.B) {
	l := &Int64List{}
	generic.Factory(List{}).Create(l)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		l.Append(int64(i))
	}
}

func BenchmarkGenericListSet(b *testing.B) {
	l := &Int64List{}
	generic.Factory(List{}).Create(l)
	l.Append(1)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		l.Set(0, 5)
	}
}

func BenchmarkGenericListGet(b *testing.B) {
	l := &Int64List{}
	generic.Factory(List{}).Create(l)
	l.Append(1)
	var v int64

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v = l.Get(0)
	}
	if v != 1 {
		panic("Slice value doesn't match expected")
	}
}

func BenchmarkSpecialisedListCreate(b *testing.B) {
	for i := 0; i < b.N; i++ {
		l := &Int32List{}
		generic.Factory(List{}).Create(l)
	}
}

func BenchmarkSpecialisedListAppend(b *testing.B) {
	l := &Int32List{}
	generic.Factory(List{}).Create(l)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		l.Append(int32(i))
	}
}

func BenchmarkSpecialisedListSet(b *testing.B) {
	l := &Int32List{}
	generic.Factory(List{}).Create(l)
	l.Append(1)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		l.Set(0, 5)
	}
}

func BenchmarkSpecialisedListGet(b *testing.B) {
	l := &Int32List{}
	generic.Factory(List{}).Create(l)
	l.Append(1)
	var v int32

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v = l.Get(0)
	}
	if v != 1 {
		panic("Slice value doesn't match expected")
	}
}

type Setter interface {
	Set(i int, v int32) int32
}

func BenchmarkInterfaceListSet(b *testing.B) {
	l := &Int32ListImpl{}
	l.Append(1)

	doBenchmarkInterfaceListSet(b, l)
}

func doBenchmarkInterfaceListSet(b *testing.B, list Setter) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		list.Set(0, 5)
	}
}

type Wrapper struct {
	Set func(i int, v int32) int32
}

func BenchmarkFuncListSet(b *testing.B) {
	l := &Int32ListImpl{}
	l.Append(1)

	list := &Wrapper{}
	list.Set = l.Set

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		list.Set(0, 5)
	}
}

func BenchmarkReflectFuncListSet(b *testing.B) {
	impl := &Int32ListImpl{}
	impl.Append(1)

	list := &Wrapper{}
	listValue := reflect.Indirect(reflect.ValueOf(list))
	implValue := reflect.ValueOf(impl)
	method := implValue.MethodByName("Set")
	listValue.FieldByName("Set").Set(method)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		list.Set(0, 5)
	}
}

func BenchmarkInterfaceSliceSet(b *testing.B) {
	slice := []interface{}{1}
	for i := 0; i < b.N; i++ {
		slice[0] = 2
	}
}

func BenchmarkPrimitiveSliceSet(b *testing.B) {
	slice := []int{1}
	for i := 0; i < b.N; i++ {
		slice[0] = 2
	}
}

func BenchmarkInterfaceSliceGet(b *testing.B) {
	slice := []interface{}{1}
	var v int

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v = slice[0].(int)
	}
	if v != 1 {
		panic("Slice value doesn't match expected")
	}
}

func BenchmarkPrimitiveSliceGet(b *testing.B) {
	slice := []int{1}
	var v int

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v = slice[0]
	}
	if v != 1 {
		panic("Slice value doesn't match expected")
	}
}

func BenchmarkReflectTypeOf(b *testing.B) {
	for i := 0; i < b.N; i++ {
		reflect.TypeOf(List{})
	}
}

func BenchmarkReflectValueOf(b *testing.B) {
	for i := 0; i < b.N; i++ {
		reflect.ValueOf(List{})
	}
}
