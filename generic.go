package generic

import (
	"fmt"
	"reflect"
)

const debug = false

var registry = make(map[reflect.Type]*Adapter)

func Factory(x interface{}) *Adapter {
	t := concrete(x)
	adapter := registry[t]
	if adapter == nil {
		adapter = &Adapter{
			from:    t,
			matches: map[reflect.Type]*Impl{},
		}
		registry[t] = adapter
	}
	return adapter
}

type Impl struct {
	adapter *Adapter

	Type   reflect.Type
	Params []reflect.Type
	New    func() interface{}

	mappings map[reflect.Type]*typeMapping
}

type typeMapping struct {
	functions []*functionMapping
}

type functionMapping struct {
	generic      reflect.StructField
	concrete     reflect.StructField
	impl         reflect.Method
	noShims      bool // All arguments match
	noConversion bool // All arguments are assignable
	inShims      []func(v reflect.Value) reflect.Value
	outShims     []func(v reflect.Value) reflect.Value
}

type Adapter struct {
	from    reflect.Type
	to      []*Impl
	matches map[reflect.Type]*Impl
}

func (a *Adapter) Create(y interface{}) {
	t := concrete(y)

	if impl, ok := a.matches[t]; ok {
		impl.adapt(y, t)
		return
	}

	params := a.params(t)
	var matchedImpl *Impl

	fmt.Println("Adapt generic type:", a.Signature(a.from))
	fmt.Println("  Specialised type:", a.Signature(t))

	// Look for exact parameter match
	for _, impl := range a.to {
		if reflect.DeepEqual(params, impl.Params) {
			matchedImpl = impl
			break
		}
	}

	// Check each implementaiton for compatibility and add to map
	if matchedImpl == nil {
		for _, impl := range a.to {
			allMatch := true

			for i, have := range impl.Params {
				want := params[i]

				if have == want ||
					want.AssignableTo(have) ||
					want.ConvertibleTo(have) {
					continue
				}

				allMatch = false
				break
			}

			if allMatch {
				matchedImpl = impl
				break
			}
		}
	}

	if matchedImpl == nil {
		panic("No implementation found for " + a.Signature(a.from))
	}

	fmt.Println("  Implementation type:", a.Signature(matchedImpl.Type))
	a.matches[t] = matchedImpl
	matchedImpl.adapt(y, t)
}

func (impl *Impl) getMapping(wrapper reflect.Type, instance reflect.Value) *typeMapping {
	mapping, ok := impl.mappings[wrapper]
	if !ok {
		mapping = &typeMapping{}

		// Map all function fields in wrapper to instance methods
		genericType := impl.adapter.from
		for i := 0; i < genericType.NumField(); i++ {
			// Find all func fields
			gField := genericType.Field(i)
			if gField.Type.Kind() != reflect.Func {
				continue
			}

			if debug {
				fmt.Println(gField)
			}

			// Find equivalent fields in wrapper and methods on impl
			// TODO support renaming via tags
			wField, ok := wrapper.FieldByName(gField.Name)
			if !ok {
				panic("No specialised method found for " + gField.Name)
			}
			if debug {
				fmt.Println(wField)
			}

			iMethod, ok := instance.Type().MethodByName(gField.Name)
			if !ok {
				panic("No implementation found for " + gField.Name)
			}
			if debug {
				fmt.Println(iMethod)
			}

			fMapping := &functionMapping{
				generic:  gField,
				concrete: wField,
				impl:     iMethod,
			}

			// Determine which shims we need
			allArgsMatch := true
			allResultsMatch := true
			allArgsAssignable := true
			allResultsAssignable := true

			inShims := make([]func(v reflect.Value) reflect.Value, gField.Type.NumIn())
			outShims := make([]func(v reflect.Value) reflect.Value, gField.Type.NumOut())

			for x := 0; x < gField.Type.NumIn(); x++ {
				haveType := iMethod.Type.In(x + 1)
				wantType := wField.Type.In(x)
				if debug {
					fmt.Println("Have:", haveType, "Want:", wantType)
				}
				if haveType != wantType {
					allArgsMatch = false

					if haveType.AssignableTo(wantType) {
						if debug {
							fmt.Println("Assignable by MakeFunc")
						}
						continue
					} else {
						allArgsAssignable = false
					}

					if haveType.ConvertibleTo(wantType) {
						if debug {
							fmt.Println("Convertible shim")
						}
						inShims[x] = func(v reflect.Value) reflect.Value {
							return v.Convert(wantType)
						}
					} else {
						if debug {
							fmt.Println("Full shim")
						}
						inShims[x] = func(v reflect.Value) reflect.Value {
							concreteValue := v
							concreteType := haveType
							if concreteType.Kind() == reflect.Interface {
								concreteValue = reflect.ValueOf(v.Interface())
								concreteType = concreteValue.Type()
							}

							if concreteType.AssignableTo(wantType) {
								return concreteValue
							}
							if debug {
								fmt.Println("Not directly assignable")
							}

							if concreteType.ConvertibleTo(wantType) {
								return concreteValue.Convert(wantType)
							}
							if debug {
								fmt.Println("Not directly convertible")
							}

							panic("Unable to convert input type")
						}
					}
				}
			}

			for x := 0; x < gField.Type.NumOut(); x++ {
				haveType := iMethod.Type.Out(x)
				wantType := wField.Type.Out(x)
				if debug {
					fmt.Println("Have:", haveType, "Want:", wantType)
				}
				if haveType != wantType {
					allResultsMatch = false

					if haveType.AssignableTo(wantType) {
						if debug {
							fmt.Println("Assignable by MakeFunc")
						}
						continue
					} else {
						allResultsAssignable = false
					}

					if haveType.ConvertibleTo(wantType) {
						if debug {
							fmt.Println("Convertible shim")
						}
						outShims[x] = func(result reflect.Value) reflect.Value {
							return result.Convert(wantType)
						}
					} else {
						if debug {
							fmt.Println("Full shim")
						}
						outShims[x] = func(result reflect.Value) reflect.Value {
							concreteValue := result
							concreteType := haveType
							if concreteType.Kind() == reflect.Interface {
								concreteValue = reflect.ValueOf(result.Interface())
								concreteType = concreteValue.Type()
							}

							if concreteType.AssignableTo(wantType) {
								return concreteValue
							}
							if debug {
								fmt.Println("Not directly assignable")
							}

							if concreteType.ConvertibleTo(wantType) {
								return concreteValue.Convert(wantType)
							}
							if debug {
								fmt.Println("Not directly convertible")
							}

							panic("Unable to convert result type")
						}
					}
				}
			}

			if allArgsMatch && allResultsMatch {
				fMapping.noShims = true
				if debug {
					fmt.Println("No shims - all args identical")
				}
			} else if allArgsAssignable && allResultsAssignable {
				fMapping.noConversion = true
				if debug {
					fmt.Println("No conversion - all args assignable")
				}
			} else {
				if debug {
					fmt.Println("Shims needed")
				}
				if !allArgsAssignable {
					fMapping.inShims = inShims
				}
				if !allResultsAssignable {
					fMapping.outShims = outShims
				}
			}

			mapping.functions = append(mapping.functions, fMapping)
		}

		impl.mappings[wrapper] = mapping
	}
	return mapping
}

func (impl *Impl) adapt(wrapper interface{}, wrapperType reflect.Type) {
	// Construct a new instance of the implementation
	instance := reflect.ValueOf(impl.New())
	wrapperValue := reflect.Indirect(reflect.ValueOf(wrapper))
	mappings := impl.getMapping(wrapperType, instance)

	// Map all function fields in wrapperType to instance methods
	for _, fMapping := range mappings.functions {

		implMethod := instance.Method(fMapping.impl.Index)

		// Check if the implMethod already has the correct signature
		if fMapping.noShims {
			wrapperValue.FieldByIndex(fMapping.concrete.Index).Set(implMethod)
			continue
		}

		// Are all types automatically assignable by MakeFunc
		if fMapping.noConversion {
			f := reflect.MakeFunc(fMapping.concrete.Type, implMethod.Call)
			wrapperValue.FieldByIndex(fMapping.concrete.Index).Set(f)
			continue
		}

		f := reflect.MakeFunc(fMapping.concrete.Type, func(args []reflect.Value) []reflect.Value {

			// TODO input shims?

			results := implMethod.Call(args)
			if fMapping.outShims == nil {
				return results
			}

			// Adapt results to correct type
			for o, result := range results {
				shim := fMapping.outShims[o]
				if shim != nil {
					results[o] = shim(result)
				}
			}
			return results
		})

		wrapperValue.FieldByIndex(fMapping.concrete.Index).Set(f)
	}
}

func (a *Adapter) Signature(t reflect.Type) string {
	params := a.params(t)
	sig := fmt.Sprintf("%s<%s>", t.String(), params)
	return sig
}

func (a *Adapter) params(n reflect.Type) []reflect.Type {
	params := []reflect.Type{}
	for i := 0; i < a.from.NumField(); i++ {
		field := a.from.Field(i)
		t := field.Type
		if t.Kind() != reflect.Func {
			nField, ok := n.FieldByName(field.Name)
			if !ok {
				panic("No impl type found for parameter " + field.Name)
			}
			nType := nField.Type
			params = append(params, nType)
		}
	}
	return params
}

func (a *Adapter) Register(impl interface{}, new func() interface{}) *Adapter {
	n := concrete(impl)
	fmt.Println("Register implementation for:", a.Signature(a.from))
	fmt.Println("        Implementation type:", a.Signature(n))

	a.to = append(a.to, &Impl{
		adapter:  a,
		Type:     n,
		Params:   a.params(n),
		New:      new,
		mappings: map[reflect.Type]*typeMapping{},
	})

	return a
}

func concrete(x interface{}) reflect.Type {
	t := reflect.TypeOf(x)
	if t.Kind() == reflect.Ptr {
		t = reflect.Indirect(reflect.ValueOf(x)).Type()
	}
	return t
}
